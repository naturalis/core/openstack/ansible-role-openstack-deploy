ansible-role-openstack-deploy
=========

A brief description of the role goes here.

Requirements
------------

- Access to Hashicorp vault
- VPN connection eduVPN (for public instances) and netDC vpn (for private instances)
- An environment variable OS_DEPLOY_KEYNAME with the name of the keypair in the deployment account
  `export OS_DEPLOY_KEYNAME=<keyname>`
- Your public key as a keypair in the deployment account (is managed through openstack projects ansible)
- The openstack.cloud collection `ansible-galaxy collection install openstack.cloud` 
- The openstack SDK version (tested with>= 0.52.0 and <=0.57.0) `sudo pip3 install openstacksdk==0.57.0`

Role Variables
--------------

Required inputs:
  - `service: ` - Name of the service to deploy, **should match the name of the openstack project name**
  - `service_owner: ` - Name ot the owner of a service, usually the contact for the service
  - `service_team: ` - Name of the team responsible for the service
  - `openstack_instance_flavor: ` - Flavor of the new instance
  - `openstack_network: `  - Name of the network used for the instance

Defaults (can be overridden locally):
  - `openstack_deploy_image_name: ubuntu-20.04.2-20210222` (Defaults to ubuntu 20.04 LTS image)
  - `openstack_deploy_keyname: ` See Requirements on how to set this
  - `openstack_data_volume_device_name: /dev/vdb` Sets the data volume device name for the local OS. Only used when `openstack_data_volume_size` is defined
  - `openstack_data_mount_path: /data` - default mound path voor extra volumes. Only used when `openstack_data_volume_size` is defined
  - `openstack_domain_name: admin_domain` - default authentication domain name
  - `openstack_auth_url: https://identity.stack.naturalis.io/v3` - default keystone authentication service in openstack

Optional:
  - `openstack_data_volume_size: ` - Size of a new volume in GB to be mounted under `openstack_data_mount_path`

Dependencies
------------
This depends on:
- Hashicorp vault
- openstack.cloud collection
- openstack client >= 5.5.0

Example config
----------------
This config deploys a **Ubuntu 20.04** with the flavor **tiny-1c.2r.20h** for **application_y** in the **public-1** network with an extra *10GB* mounted on /data. The security groups **all_sg** are set to allow **any IP** to connect on **port 80**. This is set on **all** deployed machines in the project.

```yaml
service: application_y
service_owner: Pietje Puk
service_team: Infra
openstack_instance_flavor: tiny-1c.2r.20h
openstack_network: public-1
openstack_data_volume_size: 10
openstack_security_groups:
  - name: all_sg
    apply_to: ['all'] #can be 'all' or any name of a ansible hosts group, but required
    security_rules:
    - name: allow-http-prod
      protocol: tcp #can be ' tcp','udp' or 'icmp'
      port: 80 #Any valid portnumber or -1 for any. In case of protocol ICMP, set to -1
      from: 0.0.0.0/0
```
Functions
----------------
This role allows for the following functions which are controlled by specific configurations, overall or instance specific.
- Manage instances: deploy, reconfigure, remove instances
- Manage security groups and security rules: create security groups for all or specific to a group. Add security rules for these security groups
- Manage volumes: create, extend, attach, detach and re-attach volumes

Extra data volumes options
----------------
In group_vars or host_vars you can add a variable `openstack_data_volume_size` with the size of a extra data volume for each instance (depending on were you set this variable). 
This will add an extra data volume for the instance and mounts it under `openstack_data_mount_path`  with and ext4 filesystem.

Increasing the volume size is allowed, shrinking will cause an error during deployment.

If an instance is removed or a volume is detached it can be reconnected to a new instance with the same name.

Example Playbook
----------------

```yaml
- name: Deploy openstack instances
  hosts: all
  gather_facts: false
  tags: always
  tasks:
    - name: Openstack deploy role
      include_role:
        name: openstack-deploy
```

License
-------

Apache-2.0

